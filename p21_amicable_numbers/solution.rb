require 'prime'
require 'irb'

def sum_of_divisors_of(num)
  (1..num-1).select { |n| num % n == 0}.inject(:+)
end

def sum_of_amicable_numbers_under(num=10_000)
  sum = 0
  (2..num).each do |n|
    check_sum = sum_of_divisors_of n
    sum += n if check_sum != n && sum_of_divisors_of(n) == n
  end
  sum
end

p sum_of_amicable_numbers_under 10_000
