def permutation(n)
  p = 1
  while (n != 1)
    p *= n
    n -= 1
  end
  p
end

def combination(n,r)
  p permutation(n) / ( permutation(r) * permutation(n-r) )
end

combination(4, 2)
combination(40, 20)

# n = 2y in in  y x y grid
# r = y in y x y grid
