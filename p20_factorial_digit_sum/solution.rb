def factorial(n)
  (1..n).inject(:*)
end

def sum_of_factorial(n)
  p factorial(n).to_s.split('').map(&:to_i).inject(:+)
end

sum_of_factorial 10
sum_of_factorial 100

# (1..n).inject(:*).to_s.split('').map(&:to_i).inject(:+)
