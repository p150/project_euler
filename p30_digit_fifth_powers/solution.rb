def sum_of_fifth_power_of(n)
  n.to_s.split('').map { |e| e.to_i**5 }.sum
end

def fifth_powers
  arr = []
  (2..100_000_000).each do |n|
    sum = sum_of_fifth_power_of(n)
    arr << n if sum == n
  end
  p arr.sum
end

fifth_powers
