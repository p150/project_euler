def palindrome?(n)
  n.to_s == n.to_s.reverse
end

def largest_palindrome_product(n)
  upper_lim = 10 ** n - 1
  lower_lim = 10 ** (n - 1)
  result = [1,1,1] # [x, y, product]

  upper_lim.downto(lower_lim).each do |x|
    x.downto(lower_lim).each do |y|
      prod = x * y
      if palindrome?(prod) && result[2] < prod
        result = [x, y, prod]
        return result if x < result[0] && y < result[1]
      end
    end
  end
  p result[2]
end

largest_palindrome_product 1
largest_palindrome_product 2
largest_palindrome_product 3
