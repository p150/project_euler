def two_powered(n)
  2 ** n
end

def sum_of_digits(n)
  p two_powered(n).to_s.split('').map(&:to_i).inject(:+)
end

sum_of_digits 1000
