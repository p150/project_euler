def special_triplet_product(n = 1000)
  a = (1..n/2).find { |a| (n * (n/2 - a) % (n - a)).zero? }
  b = n * (n/2 - a) / (n - a)
  c = n - a - b
  p "Product is #{a * b * (c)} where a = #{a}, b = #{b} and c = #{c}"
end

special_triplet_product
