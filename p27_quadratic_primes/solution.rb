require 'prime'

def quadratic_formula(n, a, b)
  n**2 + a*n + b
end

def prime?(n, a, b)
  quadratic_formula(n, a, b).prime?
end

def quadratic_primes(limit)
  max = [0, 0, 0]
  Prime.take_while do |b|
    (1-b..-1).each do |a|
      n = 0
      n += 1 while prime?(n, a, b)
      max = [n, a, b] if n > max[0]
    end
    b < limit
  end
  p "a: #{max[1]}, b: #{max[2]}, n: #{max[0]}, Product of 'a' and 'b': #{max[1] * max[2]}"
end

# quadratic_primes 41
# quadratic_primes 1600
# quadratic_primes 1000
quadratic_primes 1000
