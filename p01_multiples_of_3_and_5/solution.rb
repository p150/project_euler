def sum_of_multiples(limit)
  (1...limit).inject(0) do |sum, i|
    if (i % 3 == 0 || i % 5 == 0)
      sum + i
    else
      sum
    end
  end
end

p sum_of_multiples(10) # 23
p sum_of_multiples(1000) # 233168
