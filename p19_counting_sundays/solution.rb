require 'date'

def sundays_count
  @count = 0
  start_date = Date.parse('1901-1-1')
  end_date = Date.parse('2000-12-31')
  find_first_sundays(start_date, end_date)
  p "There are #{@count} Sundays that fall on the first of the month"
end

def find_first_sundays(start_date, end_date)
  (start_date..end_date).each do |day|
    @count += 1 if (day.sunday? && day.day == 1)
  end
end

sundays_count
