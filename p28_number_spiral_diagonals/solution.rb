def sum_of_diagonals_for limit
  p ((2..limit).map { |e| 2 * (e**2) }.sum) - ((1..limit-1).sum) + ((limit - 1) / 2) + 1
end

sum_of_diagonals_for 5
sum_of_diagonals_for 1001
