def distinct_powers(a, b, n)
  arr = []
  (2..n).each do |a|
    (2..n).each do |b|
      arr << a**b
    end
  end
  p arr.uniq.size
end

distinct_powers(2, 100, 100)
