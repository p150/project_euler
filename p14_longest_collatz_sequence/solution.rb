def even_operation(n)
  n / 2
end

def odd_operation(n)
  3*n + 1
end

def highest_collatz_number(num)
  max_l, max_i = 0, 0
  (num/2+1).step(num, 2).each do |i|
    l, j = 0, i
    while j != 1 do
      j = j.odd? ? odd_operation(j) : even_operation(j)
      l += 1
    end
    max_l, max_i = l, i if l > max_l
  end
  p max_i
end

highest_collatz_number(10_000_000)

