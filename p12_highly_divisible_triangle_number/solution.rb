require 'prime'

def traingle_number_of n
  (1..n).inject(:+)
end

def prime_factors n
  Prime.prime_division n
end

def prime_factors_to_all_factors(prime_factors)
  prime_factors.reduce(1) { |tot,(_,n)| tot *= (n+1) }
end

def number_of_factors_of n
  prime_factors_to_all_factors(prime_factors(n))
end

def max_triangle_number_divisors num
  n = 0
  nfacs = 0
  while nfacs < num
    n += 1
    t_num = traingle_number_of(n)
    nfacs = number_of_factors_of(t_num)
  end
  p t_num
end

max_triangle_number_divisors 5
max_triangle_number_divisors 500
