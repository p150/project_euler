require 'humanize'

def letters_count
  arr = []
  (1..1000).each do |n|
    word = n.humanize.tr_s(' -', '')
    arr << word
  end
  p arr.map { |e| e.split('') }.flatten.size
end

letters_count
# => 21124
