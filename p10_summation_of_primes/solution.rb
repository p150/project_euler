require 'prime'

def sum_of_primes_below(n)
  p Prime.take_while { |p| p < n }.inject(:+)
end

sum_of_primes_below 10
sum_of_primes_below 2_000_000
