def sum_of_squares n
  (1..n).map { |e| e**2 }.inject(:+)
end

def squares_of_sum n
  (1..n).inject(:+) ** 2
end

def difference n
  p squares_of_sum(n) - sum_of_squares(n)
end

difference 10
difference 100
