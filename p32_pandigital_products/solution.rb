init = [1, 2, 3, 4, 5, 6, 7, 8, 9]
arr = []
(1..10000).each do |mcand|
  (mcand+1..10000).each do |mplier|
    product = mcand * mplier
    break if product.size > 9
    result = "#{product}#{mcand}#{mplier}".split('').map(&:to_i).sort!
    arr << product if result == init
  end
end

p arr.uniq.inject(:+)
p arr.uniq
