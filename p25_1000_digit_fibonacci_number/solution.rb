def fib(n, memo = {})
  return n if n == 0 || n == 1
  memo[n] ||= fib(n-1, memo) + fib(n-2, memo)
end

def thou_fib_term
  num = 0
  (1..Float::INFINITY).each do |n|
    if fib(n).to_s.size > 999
      num = n
      break
    end
  end
  p num
end

thou_fib_term
