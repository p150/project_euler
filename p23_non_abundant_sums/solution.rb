upper_lim = 28_123

def abundant?(n)
  (proper_divisors(n).inject(:+) || 0) > n
end


def factors_of(n)
  f_list = []
  step = n.even? ? 1 : 2
  upper_bound = Math.sqrt(n).to_i
  (1..upper_bound).step(step).each { |i|
    if n % i == 0
      f_list << i
      f_list << n / i unless i * i == n
    end
  }
  f_list.sort
end

def proper_divisors(n)
  factors_of(n)[0..-2]
end

abundant_nums = []
(1..upper_lim).each do |n|
  abundant_nums << n if abundant?(n)
end

sums = {}
abundant_nums.each_with_index do |x, i|
  abundant_nums[i..-1].each do |y|
    sum = x + y
    break if sum > upper_lim
    sums[sum] = true
  end
end

p (1..upper_lim).reject { |el| sums[el] }.inject(:+)
