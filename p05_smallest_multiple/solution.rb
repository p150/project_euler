require 'benchmark'

def smallest_multiple_1(num)
  (1..num).inject(:lcm)
end

def smallest_multiple_2(num)
  lcm = 1
  (2..num).each do |i|
    lcm *= i / gcd(lcm, i)
  end
  lcm
end

def gcd(a, b)
  while b > 0
    a %= b
    return b if a == 0
    b %= a
  end
  a
end

smallest_multiple_1 10
smallest_multiple_1 20
smallest_multiple_2 10
smallest_multiple_2 20

Benchmark.bmbm do |bm|
  bm.report do
    smallest_multiple_1 1000
  end
  bm.report do
    smallest_multiple_1 2000
  end
  bm.report do
    smallest_multiple_2 1000
  end
  bm.report do
    smallest_multiple_2 2000
  end
end

# Rehearsal ------------------------------------
#    0.010000   0.000000   0.010000 (  0.002774)
#    0.010000   0.000000   0.010000 (  0.015893)
#    0.000000   0.000000   0.000000 (  0.001484)
#    0.000000   0.000000   0.000000 (  0.003482)
# --------------------------- total: 0.020000sec

#        user     system      total        real
#    0.000000   0.000000   0.000000 (  0.001991)
#    0.000000   0.000000   0.000000 (  0.004787)
#    0.010000   0.000000   0.010000 (  0.000970)
#    0.000000   0.000000   0.000000 (  0.003250)
